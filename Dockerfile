FROM ubuntu:18.04

# git-core package required for flit
RUN set -x \
 && apt-get update \
 && apt-get install -y \
      git-core \
      python3 \
      python3-numpy \
      python3-pip \
      python3-scipy \
      python3-venv \
 && rm -rf /var/lib/apt/lists/*

COPY requirements.txt /requirements.txt

RUN set -x \
 && python3 -m venv /venv \
 && /venv/bin/pip install --upgrade pip \
 && /venv/bin/pip install -r /requirements.txt
